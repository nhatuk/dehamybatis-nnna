package com.example.mybatis;

import com.example.mybatis.config.AppConfig;
import com.example.mybatis.service.StudentService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;


@SpringBootApplication
public class MydatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MydatisApplication.class, args);
    }

}
