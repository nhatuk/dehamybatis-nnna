package com.example.mybatis.controller.api;

import com.example.mybatis.entity.Classroom;
import com.example.mybatis.entity.Student;
import com.example.mybatis.service.ClassroomService;
import com.example.mybatis.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class StudentRestController {

    @Autowired
    StudentService studentService;

    @Autowired
    ClassroomService classroomService;

    //Danh sach student
    @GetMapping("/student")
    public ResponseEntity<List<Student>> getAllStudent(@ModelAttribute Student student)
    {
        try {
            List<Student> st = studentService.getAllStudent();
            if (st != null){
                return new ResponseEntity<>(st, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //lay ra một student
    @GetMapping("/student/{id}")
    public ResponseEntity<Optional<Student>> getOneStudent(@PathVariable("id") Integer id)
    {
        try {
            Optional<Student> student = studentService.findById(id);
            if (student.isPresent()){
                return new ResponseEntity<>(student, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT)  ;
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //Tao moi 1 hoc sinh
    @PostMapping("/student/{classroomId}")
    public ResponseEntity<Student> addStudent(@PathVariable("classroomId") Integer classroomId,
                                              @RequestBody Student student)
    {
        try {
           Classroom classroom = classroomService.findById(classroomId);
            if (classroom != null){
                Set<Classroom> classrooms = new HashSet<>();
                classrooms.add(classroomService.findById(classroomId));
                student.setClassroomId(classroomId);
                student.setClassroom(classrooms);
                studentService.insert(student);
                return new ResponseEntity<>(student,HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit student
    @PutMapping("/student/{id}")
    public ResponseEntity<Optional<Student>> editStudent(@PathVariable("id") Integer id,
                                                         @RequestBody Student students)
    {
        try {
            Optional<Student> student = studentService.findById(id);
            if (student.isPresent()){
                Student student1 = student.get();

                //set cac truong trong student entity
                student1.setStatus(students.getStatus());
                student1.setName(students.getName());
                student1.setDescription(students.getDescription());
                student1.setAge(students.getAge());
                student1.setClassroomId(students.getClassroomId());

                //Luu moi entity
                studentService.update(student1);
                return new ResponseEntity<>(student, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete student
    @DeleteMapping("student/{id}")
    public ResponseEntity<Optional<Student>> deleteStudent(@PathVariable("id") Integer id)
    {
        try {
            Optional<Student> student = studentService.findById(id);
            if (student.isPresent()){
                studentService.delete(id);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
