package com.example.mybatis.controller.api;

import com.example.mybatis.entity.Classroom;
import com.example.mybatis.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ClassRestController {

    @Autowired
    ClassroomService classroomService;

    //Danh sach class

    //lay ra 1 class
    @GetMapping("/class/{classroomId}")
    public ResponseEntity<Classroom> getOneClass(@PathVariable("classroomId") Integer classroomId){
        Classroom classroom = classroomService.findById(classroomId);
        if (classroom != null){
            return new ResponseEntity<>(classroom, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //tao class moi
    //sua class
    //xoa class

}
