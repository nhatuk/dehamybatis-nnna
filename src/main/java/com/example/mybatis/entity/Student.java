package com.example.mybatis.entity;

import java.util.Set;

public class Student {
    private Integer id;
    private String name;
    private Integer status;
    private String description;
    private Integer age;
    private Integer classroomId;
    private Set<Classroom> classroom;

    //default constructor
    public Student(){

    }

    //constructor
    public Student(Integer id, String name, Integer status, String description, Integer age, Integer classroomId, Set<Classroom> classroom) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.description = description;
        this.age = age;
        this.classroomId = classroomId;
        this.classroom = classroom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


    public Integer getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(Integer classroomId) {
        this.classroomId = classroomId;
    }

    public Set<Classroom> getClassroom() {
        return classroom;
    }

    public void setClassroom(Set<Classroom> classroom) {
        this.classroom = classroom;
    }
}
