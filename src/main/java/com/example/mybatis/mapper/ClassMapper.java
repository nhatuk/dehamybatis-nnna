package com.example.mybatis.mapper;

import com.example.mybatis.entity.Classroom;
import java.util.List;

public interface ClassMapper {

    //insert classroom
    void insert(Classroom classroom);

    //delete classroom
    void delete(Integer id);

    //update classroom
    void update(Classroom classroom);

    //tim kiem classroom
    Classroom findById(Integer classroomId);

    //danh sach classroom
    List<Classroom> getAllClass();

}
