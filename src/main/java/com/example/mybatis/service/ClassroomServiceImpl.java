package com.example.mybatis.service;

import com.example.mybatis.entity.Classroom;
import com.example.mybatis.mapper.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClassroomServiceImpl implements ClassroomService {

    @Autowired
    ClassMapper classMapper;

    //insert classroom
    @Override
    public void insert(Classroom classroom) {
        classMapper.insert(classroom);
    }

    //delete classroom
    @Override
    public void delete(Integer id) {
        classMapper.delete(id);
    }

    //update classroom
    @Override
    public void update(Classroom classroom) {
        classMapper.update(classroom);
    }

    //tim kiem 1 classroom
    @Override
    public Classroom findById(Integer classroomId) {
        return classMapper.findById(classroomId);
    }

    //danh sach classroom
    @Override
    public List<Classroom> getAllClass() {
        List<Classroom> cl = classMapper.getAllClass();
        return cl;
    }
}
