package com.example.mybatis.service;

import com.example.mybatis.entity.Student;
import com.example.mybatis.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service(value = "studentService")
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentMapper studentMapper;

    //insert data
    @Override
    public void insert(Student student) {
        studentMapper.insert(student);
    }

    //delete data
    @Override
    public void delete(Integer id) {
        studentMapper.delete(id);
    }

    //update data
    @Override
    public void update(Student student) {
        studentMapper.update(student);
    }

    //tim 1 data
    @Override
    public Optional<Student> findById(Integer id) {
        return studentMapper.findById(id);
    }

    //danh sach data
    @Override
    public List<Student> getAllStudent() {
        List<Student> st = studentMapper.getAllStudent();
        return st;
    }
}
