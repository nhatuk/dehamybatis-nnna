package com.example.mybatis.service;


import com.example.mybatis.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    //insert data
    void insert(Student student);

    //Delete data
    void delete(Integer id);

    //Luu data
    void update(Student student);

    //tim bang id
    Optional<Student> findById(Integer id);

    //All queries
    List<Student> getAllStudent();
}
