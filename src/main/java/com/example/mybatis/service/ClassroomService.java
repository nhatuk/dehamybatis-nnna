package com.example.mybatis.service;

import com.example.mybatis.entity.Classroom;
import java.util.List;

public interface ClassroomService {

    //insert classroom
    public void insert(Classroom classroom);

    //delete classroom
    public void delete(Integer id);

    //update classroom
    public void update(Classroom classroom);

    // tim 1 classroom
    public Classroom findById(Integer classroomId);

    // danh sach classroom
    public List<Classroom> getAllClass();
}
